require 'spec_helper'
require_relative '../../../../apps/api/controllers/fight/remove'

describe Api::Controllers::Fight::Remove do
  let(:action) { Api::Controllers::Fight::Remove.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
