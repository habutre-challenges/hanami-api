require 'spec_helper'
require_relative '../../../../apps/api/controllers/fight/retrieve'

describe Api::Controllers::Fight::Retrieve do
  let(:action) { Api::Controllers::Fight::Retrieve.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
