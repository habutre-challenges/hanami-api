require 'spec_helper'
require_relative '../../../../apps/api/controllers/fight/create'

describe Api::Controllers::Fight::Create do
  let(:action) { Api::Controllers::Fight::Create.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
