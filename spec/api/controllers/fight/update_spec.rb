require 'spec_helper'
require_relative '../../../../apps/api/controllers/fight/update'

describe Api::Controllers::Fight::Update do
  let(:action) { Api::Controllers::Fight::Update.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
