require 'spec_helper'
require_relative '../../../../apps/api/controllers/fight/index'

describe Api::Controllers::Fight::Index do
  let(:action) { Api::Controllers::Fight::Index.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
