delete  '/fight/:id',  to: 'fight#remove'
put     '/fight/:id',  to: 'fight#update'
post    '/fight',      to: 'fight#create'
get     '/fight/:id',  to: 'fight#retrieve'
get     '/fight',      to: 'fight#index'
# Configure your routes here
# See: http://www.rubydoc.info/gems/hanami-router/#Usage
