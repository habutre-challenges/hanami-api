module Api::Controllers::Fight
  class Index
    include Api::Action

    def call(params)
      self.body = 'OK'
    end
  end
end
